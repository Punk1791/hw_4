using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;

namespace hw_4
{
    public class CustomCsv<T> : ISerealise where T: class, new()
    {
        public void SerealizeToFile(object obj)
        {
            List<string> data = new();

            try
            {
                foreach (var p in GetProperties())
                {
                    var proper = p.GetValue(obj);
                    data.Add(proper == null ? "" : proper.ToString());
                }

                using (StreamWriter sw = new StreamWriter("test.txt"))
                {
                    sw.WriteLine(string.Join(";",data));
                }
            }
            catch
            {
                Console.WriteLine("Неверный тип данных");
            }

        }


        public void DeseraliseToFile()
        {
            try
            {
                string obj;
                using (StreamReader sw = new StreamReader("test.txt"))
                {
                    obj = sw.ReadLine();
                }
                var spl = obj.Split(";");

                var data = new T();
                var properties = GetProperties();
                var i = 0;
                foreach (var p in properties)
                {
                    p.SetValue(data,Convert.ChangeType(spl[i++], p.PropertyType), null);
                }
            }
            catch
            {
                Console.WriteLine("Неверный тип данных");
            }
        }

        private PropertyInfo[] GetProperties()
        {
            return typeof(T)
                        .GetProperties(BindingFlags.Public | BindingFlags.Instance | BindingFlags.GetProperty | BindingFlags.SetProperty)
                        .ToArray();
        }
    }
}