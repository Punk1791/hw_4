using System;
using System.IO;
using System.Reflection;

namespace hw_4
{
    public class RunSerealise<T> where T : ISerealise
    {
        public TimeSpan Run(T data)
        {
            F ff = new();
            const int n = 100000;
            var at = DateTime.Now;
            for (int i = 0; i < n; i++)
            {
                data.SerealizeToFile(ff.Get());
            }
            var to = DateTime.Now;

            var ret = (to-at)/n;
            Console.WriteLine($"Для сереализации понадобилось, в среднем, {ret}");

            at = DateTime.Now;

            Console.WriteLine($"Для вывода текста понадобилось {at-to}");

            return ret;
        }

    }
}