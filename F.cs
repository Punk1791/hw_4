﻿using System;

namespace hw_4
{

    public class F 
    { 
        public int i1 {get;set;} 
        public int i2 {get;set;} 
        public int i3 {get;set;} 
        public int i4 {get;set;} 
        public int i5 {get;set;} 

        public F Get() => new F(){ i1 = 1, i2 = 2, i3 = 3, i4 = 4, i5 = 5 };

        public string ToCsv()
        {
            return $"i1={i1};i2={i2};i3={i3};i4={i4};i5={i5};";
        }

        public string ToJson()
        {
            return "{" + $"i1={i1},i2={i2},i3={i3},i4={i4},i5={i5}" + "}";
        }
    }
}