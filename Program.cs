﻿namespace hw_4
{
    public static class Program
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("Самодельный механизм");
            var runnerCsv = new RunSerealise<CustomCsv<F>>().Run(new CustomCsv<F>());
            Console.WriteLine("Стандартный механизм");
            var runnerJson = new RunSerealise<JsonSerealise>().Run(new JsonSerealise());
            Console.WriteLine($"Разница во времени между самодельным и стандартным механизмом, в среднем, составляет {runnerCsv-runnerJson}");
        }
    }
}