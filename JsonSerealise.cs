using System;
using System.IO;
using Newtonsoft.Json;

namespace hw_4
{
    public class JsonSerealise : ISerealise
    {
        public void SerealizeToFile(object obj)
        {
            if (obj.GetType() == typeof(F))
            {
                using (StreamWriter sw = new StreamWriter("test.txt"))
                {
                    sw.WriteLine(JsonConvert.SerializeObject(obj));
                }
            }
            else
            {
                Console.WriteLine("Неверный тип данных");
            }
        }


        public void DeseraliseToFile()
        {
            using (StreamReader sw = new StreamReader("test.txt"))
            {
                var line = sw.ReadLine();
                try
                {
                    var Deseralise = JsonConvert.DeserializeObject<F>(line);
                }
                catch
                {
                    Console.WriteLine("Неверный тип данных");
                }
            }
        }
    }
}